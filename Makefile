all: genimg

compile:
	mkdir -p bin
	utils/compile.sh

genimg: compile
	utils/genimg.sh

test:
	qemu-system-x86_64 -hda /dev/null -m 128 -kernel bzImage -initrd initramfs.cpio.gz -append 'quiet rdinit=/main'

clean:
	cat .gitignore | xargs rm -rfv
