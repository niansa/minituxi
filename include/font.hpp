#include <span>



namespace Font {
constexpr unsigned int height = 16,
                       width = 8;
constexpr unsigned int size = height * width;

using Letter = std::span<const bool>;


extern const bool fontBitmapUnk[size];



Letter getChar(const char character);
inline Letter getUnk() {
    return Letter{fontBitmapUnk, size};
}
}
