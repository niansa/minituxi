#ifndef _SYSTEM_HPP
#define _SYSTEM_HPP
#include <iostream>
#include <string_view>
#include <fstream>
#include <filesystem>
#include <functional>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/sysinfo.h>
#include <sys/mount.h>
#include <sys/mman.h>
#include <linux/fb.h>
#include <linux/input.h>



namespace System {
[[noreturn]]
void lockup();

[[noreturn]]
void panic(std::string_view msg);

template<typename T>
inline T check_sysc(T v, std::string_view msg = "Syscall failed") {
    if (v < 0) {
        panic(msg);
    }
    return v;
}



class SysInfo {
    struct sysinfo i;

public:
    SysInfo() {
        check_sysc(sysinfo(&i), "Failed to obtain sysinfo");
    }

    const struct sysinfo& get() const {
        return i;
    }
};


class Mounts {
public:
    Mounts() {
        std::filesystem::create_directory("/dev");
        std::filesystem::create_directory("/proc");
        check_sysc(mount("devtmpfs", "/dev", "devtmpfs", 0, 0), "Failed to initialize devices filesystem");
        check_sysc(mount("proc", "/proc", "proc", 0, 0), "Failed to initialize process filesystem");
    }

    ~Mounts() {
        check_sysc(umount("/dev"), "Failed to deinitialize devices filesystem");
        check_sysc(umount("/proc"), "Failed to deinitialize process filesystem");
    }

};



class TermAntiFlagger {
    termios termbak;
public:
    TermAntiFlagger(tcflag_t antiflags) {
        termios thisterm;
        tcgetattr(0, &thisterm);
        termbak = thisterm;
        thisterm.c_lflag &= ~antiflags;
        tcsetattr(0, TCSANOW, &thisterm);
    }
    ~TermAntiFlagger() {
        tcsetattr(0, TCSANOW, &termbak);
    }
};



struct Size {
    uint width, height;
};
struct Coord {
    uint x, y;
};
struct Rect {
    Coord first, second;
};


class Framebuffer {
    using pixType = uint32_t;

    struct fb_fix_screeninfo finfo;
    struct fb_var_screeninfo vinfo;
    int fb_fd;
    pixType *buf; // Slow real buffer
    pixType *dBuf; // Fast double buffer
    size_t bufSize; // In bytes!!!
    size_t bufArrSize; // In pixels

public:
    Framebuffer() {
        // Open framebuffer
        fb_fd = check_sysc(open("/dev/fb0", O_RDWR), "Unable to access framebuffer");
        // Collect information
        ioctl(fb_fd, FBIOGET_VSCREENINFO, &vinfo);
        ioctl(fb_fd, FBIOGET_FSCREENINFO, &finfo);
        bufArrSize = vinfo.xres * vinfo.yres;
        bufSize = sizeof(pixType) * bufArrSize;
        // Map memory
        buf = reinterpret_cast<pixType*>(mmap(nullptr, bufSize, PROT_READ | PROT_WRITE, MAP_SHARED, fb_fd, 0));
        dBuf = new pixType[bufSize];
    }
    ~Framebuffer() {
        // Unmap memory
        munmap(buf, bufSize);
        delete []dBuf;
        // Close framebuffer
        close(fb_fd);
    }

    const auto& get_finfo() const {
        return finfo;
    }
    const auto& get_vinfo() const {
        return vinfo;
    }

    Rect get_centered(Size s) {
        Coord rad;
        rad.x = vinfo.xres / 2 - s.width / 2;
        rad.y = vinfo.yres / 2 - s.height / 2;
        return {
            {vinfo.xres / 2 - s.width / 2, vinfo.yres / 2 - s.height / 2},
            {vinfo.xres / 2 + s.width / 2, vinfo.yres / 2 + s.height / 2}
        };
    }
    Coord get_center() {
        return {vinfo.xres / 2, vinfo.yres / 2};
    }

    bool inBounds(size_t idx) {
        return idx < bufArrSize;
    }

    pixType& operator[](size_t idx) {
        return dBuf[idx];
    }

    void display() {
        memcpy(buf, dBuf, bufSize);
    }

    void draw(size_t idx, pixType color) {
        if (inBounds(idx)) {
            dBuf[idx] = color;
        }
    }
    void draw(Coord c, pixType color) {
        if (c.x < vinfo.xres && c.y < vinfo.yres) {
            dBuf[c.y * vinfo.xres + c.x] = color;
        }
    }

    void fill(pixType color) {
        for (size_t idx = 0; inBounds(idx); idx++) {
            dBuf[idx] = color;
        }
    }

    void draw_text(std::string_view str, Coord coord, pixType color = 0xFFFFFF, bool centered = false);

    void draw_horizontal_line(uint x1, uint x2, uint y, pixType color) {
        for (uint i = x1; i < x2; i++) {
            draw({i, y}, color);
        }
    }
    void draw_vertical_line(uint x, uint y1, uint y2, pixType color) {
        for (uint i = y1; i < y2; i++) {
            draw({x, i}, color);
        }
    }
    void draw_line(const Rect& r, pixType color);

    void draw_circle(double cx, double cy, uint radius, pixType color, bool filled = false);

    void draw_rect(const Rect& r, pixType color) {
        // Individual lines
        draw_line({r.first, {r.second.x, r.first.y}}, color);
        draw_line({r.first, {r.first.x, r.second.y}}, color);
        draw_line({{r.second.x, r.first.y}, r.second}, color);
        draw_line({{r.first.x, r.second.y}, r.second}, color);
    }
    void draw_rect_filled(const Rect& r, pixType color);
};


class Mouse {
    int mouse_fd;

public:
    struct Event {
        struct input_event base;

        enum {
            MOUSEX,
            MOUSEY,
            WHEEL,
            BUTTON
        } type;

        int32_t value;

        Event(struct input_event& base) : base(base) {
            switch (base.type) {
            case 2: {
                switch (base.code) {
                case 0: type = MOUSEX; break;
                case 1: type = MOUSEY; break;
                case 11: type = WHEEL; break;
                }
            } break;
            case 4: type = BUTTON; break;
            }
            value = base.value;
        }
    };

    Mouse() {
        // Find mouse
        std::filesystem::path mouseEventPath;
        for (const auto& f : std::filesystem::directory_iterator("/dev/input/by-path/")) {
            auto& path = f.path();
            if (f.path().filename().generic_string().ends_with("-event-mouse")) {
                mouseEventPath = f.path();
            }
        }
        // Check that a mouse was found
        if (mouseEventPath.empty()) {
            mouse_fd = -1;
        } else {
            // Open mice
            mouse_fd = check_sysc(open(mouseEventPath.c_str(), O_RDWR), "Unable to access mouse");
            // Gain exclusive access
            check_sysc(ioctl(mouse_fd, EVIOCGRAB, 1), "Failed to gain exclusive access over mouse");
        }
    }

    bool connected() {
        return mouse_fd != -1;
    }

    Event event_wait() {
        if (!connected()) {
            lockup();
        }
        struct input_event ev;
        read(mouse_fd, &ev, sizeof(ev));
        return Event(ev);
    }
};
}
#endif
