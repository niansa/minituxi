#include "system.hpp"
#include "font.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <exception>
#include <thread>
#include <chrono>



class ProgressWindow {
    System::Rect wRect;
    System::Rect barRect;
    uint barX = 0;
    uint textX;

public:
    std::string text = "Loading...";
    ProgressWindow(const System::Rect& wRect) : wRect(wRect) {
        barRect = {wRect.first.x+10, wRect.first.y+10, wRect.second.x-10, wRect.first.y+20};
        textX = wRect.first.x+(wRect.second.x-wRect.first.x)/2;
    }

    void draw(System::Framebuffer& fb) const {
        fb.draw_rect_filled(wRect, 0xFFFFFF); // Window
        fb.draw_text(text, {textX, wRect.second.y-Font::height}, 0x000000, true); // Text
        fb.draw_rect_filled({barRect.first, {barRect.first.x+barX, barRect.second.y}}, 0x0000FF); // Progress bar
        fb.draw_rect(barRect, 0x000000); // Progress bar outline
    }

    void updateProgress(uint8_t percentage) {
        if (percentage <= 100) {
            barX = (barRect.second.x-barRect.first.x)/100.f*percentage;
        }
    }
};


struct UMain {
    std::mutex mutex;
    System::Framebuffer fb;
    System::Mouse mouse;
    System::Coord mousePos;

    void pollLoop() {
        while (true) {
            // Update mouse position
            auto ev = mouse.event_wait();
            mutex.lock();
            switch (ev.type) {
            case System::Mouse::Event::MOUSEX: mousePos.x += ev.value; break;
            case System::Mouse::Event::MOUSEY: mousePos.y += ev.value; break;
            default: break;
            }
            mutex.unlock();
        }
    }

    void drawLoop() {
        using namespace std::literals::chrono_literals;
        ProgressWindow pWin(fb.get_centered({500, 40}));
        uint8_t p = 0;
        while (true) {
            mutex.unlock();
            std::this_thread::sleep_for(15ms);
            mutex.lock();
            // Clear screen
            fb.fill(0x000000);
            // Some window with a progress bar
            if (p != 100) {
                pWin.text = "Loading... "+std::to_string(p)+"%";
                pWin.draw(fb);
                pWin.updateProgress(p++);
            } else {
                // Draw mouse
                fb.draw_circle(mousePos.x, mousePos.y, 5, 0xFFFFFF, true);
                fb.draw_text("This is the cursor", {mousePos.x + 20, mousePos.y - Font::height / 2}, 0x00FF00);
                fb.draw_line({{0, 0}, mousePos}, 0xFF00FF);
            }
            // Display
            fb.display();
        }
    }

    void run() {
        std::thread pollThread([this] () {
            pollLoop();
        });
        drawLoop();
    }
};


int main() {
    std::cout << "\033[H\033[J" << std::flush;

    // Set terminate handle
    std::set_terminate([] () {
        try {
            std::rethrow_exception(std::current_exception());
        } catch (std::exception& e) {
            System::panic(e.what());
        } catch (...) {
            System::panic("Unknown");
        }
    });

    // Initialisations if PID 1 then run
    if (getpid() == 1)
        new System::Mounts;
    UMain().run();

    // Lock up
    System::lockup();
}
