#include "system.hpp"
#include "font.hpp"



namespace System {
[[noreturn]]
void lockup() {
    std::cout << std::flush;
    std::cerr << std::flush;
    if (getpid() == 1) {
        while(1) usleep(-1);
    } else {
        abort();
    }
}

[[noreturn]]
void panic(std::string_view msg) {
    std::cerr << "\n\n\n"
                 "An unrecoverable userspace error has occurred: \n"
                 "Message: " << msg << "\n"
                 "Errno: " << strerror(errno) << std::endl;
    lockup();
}


void Framebuffer::draw_text(std::string_view str, Coord coord, pixType color, bool centered) {
    // Get half pixel width if centered
    if (centered) {
        coord.x -= (str.size() * Font::width) / 2;
    }
    // Iterate through string
    for (const auto c : str) {
        // Get bitmap
        auto bitmap = Font::getChar(c);
        // Render
        Coord r = {0,0};
        for (const auto pixel : bitmap) {
            if (r.x == Font::width) {
                r.x = 0;
                r.y++;
            }
            if (r.y == Font::height) {
                break;
            }
            if (pixel) {
                draw({coord.x + r.x, coord.y + r.y}, color);
            }
            r.x++;
        }
        // Increase x
        coord.x += Font::width;
    }
}

void Framebuffer::draw_line(const Rect& r, pixType pixel) {
    // Deltas
    int dx = r.second.x - r.first.x;
    int dy = r.second.y - r.first.y;
    // Absolute deltas
    uint dxabs = abs(dx);
    uint dyabs = abs(dy);
    // Signed deltas
    int sdx = (dx>0)?1:-1;
    int sdy = (dy>0)?1:-1;
    // Misc stuff
    int x = dyabs / 2;
    int y = dxabs / 2;
    int px = r.first.x;
    int py = r.first.y;

    if (dxabs>=dyabs) {
        for (int i = 0; i < dxabs; i++) {
            y += dyabs;
            if (y >= dxabs) {
                y -= dxabs;
                py += sdy;
            }
            px += sdx;
            draw({static_cast<uint>(px), static_cast<uint>(py)}, pixel);
        }
    } else {
        for (int i = 0; i < dyabs; i++) {
            x += dxabs;
            if (x >= dyabs) {
                x -= dyabs;
                px += sdx;
            }
            py += sdy;
            draw({static_cast<uint>(px), static_cast<uint>(py)}, pixel);
        }
    }
}

void Framebuffer::draw_circle(double cx, double cy, uint radius, pixType color, bool filled) {
    auto plot4points = [&] (double x, double y) {
        if (filled) {
            draw_horizontal_line(cx - x, cx + x, cy + y, color);
            draw_horizontal_line(cx - x, cx + x, cy - y, color);
        } else {
            draw({static_cast<uint>(cx + x), static_cast<uint>(cy + y)}, color);
            draw({static_cast<uint>(cx - x), static_cast<uint>(cy + y)}, color);
            draw({static_cast<uint>(cx + x), static_cast<uint>(cy - y)}, color);
            draw({static_cast<uint>(cx - x), static_cast<uint>(cy - y)}, color);
        }
    };
    auto plot8points = [&] (double x, double y) {
        plot4points(x, y);
        plot4points(y, x);
    };

    int error = -radius;
    double x = radius;
    double y = 0;

    while (x >= y) {
        plot8points(x, y);

        error += y;
        y++;
        error += y;

        if (error >= 0) {
            error += -x;
            x--;
            error += -x;
        }
    }
}

void Framebuffer::draw_rect_filled(const Rect &r, pixType color) {
    for (auto p = r.first;;) {
        draw({p.x, p.y}, color);
        if (p.x++ == r.second.x) {
            p.x = r.first.x;
            if (p.y++ == r.second.y) {
                break;
            }
        }
    }
}
}
