#! /bin/sh


# Config
C="c++"
CFLAGS="-std=c++20 -static -O2 -I../include"

# Init env
cd src

# Clean up old executables
rm -f ../bin/*

# Compile executables
cd ../src
outfile="../bin/main"
"$C" $CFLAGS *.cpp -o "$outfile" || exit
