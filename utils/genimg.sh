#! /bin/sh
cd bin
ls ./* | cpio -ov -H newc -R 0 --no-absolute-filenames > ../initramfs.cpio || exit
cd ..
gzip -f initramfs.cpio
